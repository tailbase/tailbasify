﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tailbasify.Datatail.TailbaseCoreDb;

namespace Tailbasify.Datatail.Repositories
{
    public class ShopifyMerchantRepository : BaseRepository
    {
        public ShopifyMerchant GetMerchant(int merchantId)
        {
            return coreContext.ShopifyMerchants.Where(sm => sm.MerchantId == merchantId)
                .FirstOrDefault();
        }

        public ShopifyMerchant GetMerchant(string shop)
        {
            return coreContext.ShopifyMerchants.Where(sm => sm.Shop.Equals(shop))
                .FirstOrDefault();
        }

        public void CreateMerchant(int merchantId, string shop, bool isDemo)
        {
            ShopifyMerchant merchant = new ShopifyMerchant()
            {
                MerchantId = merchantId,
                Shop = shop,
                Demo = isDemo,
                Live = false,
            };

            coreContext.ShopifyMerchants.Add(merchant);
            coreContext.SaveChanges();
        }

        public void SaveToken(string shop, string accessToken)
        {
            ShopifyMerchant merchant = GetMerchant(shop);
            if (merchant != null)
                merchant.Token = accessToken;

            coreContext.SaveChanges();
        }

        public string GetShopifyStoreURL(int merchantId)
        {
            return coreContext.ShopifyMerchants.Where(sm => sm.MerchantId == merchantId)
                .FirstOrDefault()
                .Shop;
        }
    }
}
