﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tailbasify.Datatail.TailbaseCoreDb;

namespace Tailbasify.Datatail.Repositories
{
    public class MerchantWebsiteTextsRepository : BaseRepository
    {
        public MerchantWebsiteText GetMerchantWebsiteText(int merchantId, string textCode)
        {
            return coreContext.MerchantWebsiteTexts
                .Where(t => t.merchant_id == merchantId && t.textcode.Equals(textCode))
                .FirstOrDefault();
        }

        public int GetMerchantId(string textCode, string content)
        {
            MerchantWebsiteText websiteText = coreContext.MerchantWebsiteTexts
                .Where(t => t.textcode.Equals(textCode) && t.content_1.Contains(content))
                .FirstOrDefault();

            if (websiteText == null)
                return 0;
            else
                return websiteText.merchant_id;
        }

        public void CreateMercahntWebsiteText(int merchantId, string textCode, string content1, string content2 = "")
        {
            MerchantWebsiteText webisteText = new MerchantWebsiteText()
            {
                merchant_id = merchantId,
                textcode = textCode,
                content_1 = content1,
                content_2 = (content2 == "" || content2 == null) ? content1 : content2,
                CreationDate = DateTime.Now,
                ModificationDate = DateTime.Now
            };

            coreContext.MerchantWebsiteTexts.Add(webisteText);
            coreContext.SaveChanges();

        }

        public void UpdateMerchantWebsiteText(int merchantId, string textCode, string content1, string content2 = "")
        {
            MerchantWebsiteText webisteText = GetMerchantWebsiteText(merchantId, textCode);
            if (webisteText == null)
            {
                CreateMercahntWebsiteText(merchantId, textCode, content1, content2);
            }
            else
            {
                webisteText.content_1 = content1;
                webisteText.content_2 = content2 == "" ? content1 : content2;
                coreContext.SaveChanges();
            }
        }
    }
}
