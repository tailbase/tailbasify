﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tailbasify.Datatail.TailbaseCoreDb;

namespace Tailbasify.Datatail.Repositories
{
    public class MerchantRepository : BaseRepository
    {
        
        public merchant GetMerchant(int merchantId)
        {
            return coreContext.merchants.Where(m => m.ID == merchantId)
                .FirstOrDefault();
        }

        public merchant GetMerchant(string url)
        {
            return coreContext.merchants.Where(m => m.merchant_url.Equals(url))
                .FirstOrDefault();
        }

        public void UpdateCanonicalUrl(int merchantId, string shop)
        {
            merchant m = GetMerchant(merchantId);
            if(m != null)
            {
                m.CanonicalUrl = shop;
                coreContext.SaveChanges();
            }
        }
    }
}
