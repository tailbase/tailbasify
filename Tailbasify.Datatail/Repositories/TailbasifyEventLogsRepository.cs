﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tailbasify.Datatail.TailbaseCoreDb;

namespace Tailbasify.Datatail.Repositories
{
    public class TailbasifyEventLogsRepository : BaseRepository
    {
        public void AddLog(TailbasifyEventLog log)
        {
            coreContext.TailbasifyEventLogs.Add(log);
            coreContext.SaveChanges();
        }
    }
}
