﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tailbasify.Datatail.TailbaseCoreDb;

namespace Tailbasify.Datatail.Repositories
{
    public class BaseRepository
    {
        public TailbaseCoreDbContext coreContext = new TailbaseCoreDbContext(ConfigurationManager.ConnectionStrings["TailbaseCoreDbContext"].ConnectionString);

    }
}
