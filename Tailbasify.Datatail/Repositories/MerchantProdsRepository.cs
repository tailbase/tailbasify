﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tailbasify.Datatail.TailbaseCoreDb;

namespace Tailbasify.Datatail.Repositories
{
    public class MerchantProdsRepository : BaseRepository
    {
        public List<ShopifyMerchantProdsExtended> GetProducts(int? merchantId)
        {
            return coreContext.ShopifyMerchantProdsExtendeds.Where(mp => mp.Merchant_ID == merchantId).Take(500).ToList();
        }

        public void UpdateShopifyProductId(int merchantId, Dictionary<int, long> productIdsMap)
        {
            List<int> merchantProdIds = productIdsMap.Keys.ToList();
            List<MerchantProd> toUpdate = coreContext.MerchantProds
                .Where(mp => mp.Merchant_ID == merchantId
                        && merchantProdIds.Contains(mp.ProductID)).ToList();

            foreach(MerchantProd mp in toUpdate)
            {
                mp.ShopifyProductId = productIdsMap[mp.ProductID];
            }

            coreContext.SaveChanges();
        }
    }
}
