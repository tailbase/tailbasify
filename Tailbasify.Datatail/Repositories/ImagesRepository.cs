﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tailbasify.Datatail.TailbaseCoreDb;

namespace Tailbasify.Datatail.Repositories
{
    public class ImagesRepository : BaseRepository
    {
        public List<ItemImagesHD> GetImages(List<int> productIDs)
        {
            return coreContext.ItemImagesHDs.Where(i => productIDs.Contains(i.ItemID) && i.Type == 1).ToList();
        }
    }
}
