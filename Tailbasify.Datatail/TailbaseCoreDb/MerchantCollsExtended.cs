//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class MerchantCollsExtended
    {
        public string collection_1 { get; set; }
        public string collection_2 { get; set; }
        public Nullable<decimal> price { get; set; }
        public Nullable<decimal> cost { get; set; }
        public Nullable<decimal> reducedPrice { get; set; }
        public Nullable<decimal> advPrice { get; set; }
        public bool advertised { get; set; }
        public bool featured { get; set; }
        public bool @new { get; set; }
        public bool refurbished { get; set; }
        public bool specialBuy { get; set; }
        public bool webonly { get; set; }
        public bool storeonly { get; set; }
        public bool liquidation { get; set; }
        public Nullable<int> liquidation_storeID { get; set; }
        public bool demo { get; set; }
        public string va_1 { get; set; }
        public string va_2 { get; set; }
        public int collectionID { get; set; }
        public string ptr { get; set; }
        public int deptID { get; set; }
        public int Merchant_ID { get; set; }
        public Nullable<int> liquidation_qty { get; set; }
        public Nullable<int> qty { get; set; }
        public string description_1 { get; set; }
        public string description_2 { get; set; }
        public string defaultDesc_1 { get; set; }
        public string defaultDesc_2 { get; set; }
        public bool featured_national { get; set; }
    }
}
