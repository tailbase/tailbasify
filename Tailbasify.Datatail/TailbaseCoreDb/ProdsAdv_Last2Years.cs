//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProdsAdv_Last2Years
    {
        public Nullable<decimal> AdvPrice { get; set; }
        public Nullable<int> scanID { get; set; }
        public int productId { get; set; }
        public Nullable<System.DateTime> DateF { get; set; }
        public int RetailerID { get; set; }
        public int catID { get; set; }
        public int ID { get; set; }
    }
}
