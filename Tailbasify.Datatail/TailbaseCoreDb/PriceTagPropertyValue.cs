//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class PriceTagPropertyValue
    {
        public int Id { get; set; }
        public int PriceTagId { get; set; }
        public int PriceTagPropertyId { get; set; }
        public string Value { get; set; }
    
        public virtual PriceTagProperty PriceTagProperty { get; set; }
        public virtual PriceTags1 PriceTags1 { get; set; }
    }
}
