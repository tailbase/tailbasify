//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class MerchantShippingPricesByQuantity
    {
        public int Id { get; set; }
        public int MerchantShippingId { get; set; }
        public Nullable<int> MerchantZoneId { get; set; }
        public int FromQuantity { get; set; }
        public Nullable<int> ToQuantity { get; set; }
        public decimal BasePrice { get; set; }
        public decimal PerItemPrice { get; set; }
    
        public virtual MerchantShipping MerchantShipping { get; set; }
        public virtual MerchantZone MerchantZone { get; set; }
    }
}
