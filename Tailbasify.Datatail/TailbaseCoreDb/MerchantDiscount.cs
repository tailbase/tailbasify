//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class MerchantDiscount
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MerchantDiscount()
        {
            this.MerchantDiscountGroups = new HashSet<MerchantDiscountGroup>();
            this.MerchantDiscountProducts = new HashSet<MerchantDiscountProduct>();
        }
    
        public int Merchant_ID { get; set; }
        public string Code_disc { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Message_1 { get; set; }
        public string Message_2 { get; set; }
        public Nullable<decimal> Disc_flat { get; set; }
        public Nullable<decimal> Disc_perc { get; set; }
        public Nullable<bool> Active { get; set; }
        public Nullable<int> Times_used { get; set; }
        public Nullable<int> Max_use { get; set; }
        public string ApplyType { get; set; }
        public string ApplyTarget { get; set; }
        public decimal ApplyThreshold { get; set; }
        public bool showOnProductPages { get; set; }
        public string shortProductPagesText_1 { get; set; }
        public string longProductPagesText_1 { get; set; }
        public string shortProductPagesText_2 { get; set; }
        public string longProductPagesText_2 { get; set; }
        public Nullable<decimal> MaxDiscAmount { get; set; }
        public Nullable<decimal> ApplyMaxCartLimit { get; set; }
        public Nullable<decimal> MinDiscAmount { get; set; }
        public bool StorePickupOnly { get; set; }
        public bool DeliveryOnly { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MerchantDiscountGroup> MerchantDiscountGroups { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MerchantDiscountProduct> MerchantDiscountProducts { get; set; }
    }
}
