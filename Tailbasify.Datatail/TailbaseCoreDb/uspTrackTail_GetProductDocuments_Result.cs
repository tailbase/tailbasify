//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    
    public partial class uspTrackTail_GetProductDocuments_Result
    {
        public int Id_Product { get; set; }
        public string folder { get; set; }
        public string url { get; set; }
    }
}
