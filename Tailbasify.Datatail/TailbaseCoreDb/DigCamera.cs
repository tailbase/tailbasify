//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class DigCamera
    {
        public int Id_product { get; set; }
        public string model { get; set; }
        public int manufID { get; set; }
        public Nullable<int> subCatID { get; set; }
        public bool active { get; set; }
        public bool discontinued { get; set; }
        public Nullable<System.DateTime> DateDisc { get; set; }
    }
}
