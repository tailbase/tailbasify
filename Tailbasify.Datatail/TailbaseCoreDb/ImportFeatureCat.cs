//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class ImportFeatureCat
    {
        public int id { get; set; }
        public int ImportBrandId { get; set; }
        public string CatEN { get; set; }
        public string CatFR { get; set; }
        public Nullable<int> catID { get; set; }
        public Nullable<int> subcatID { get; set; }
    
        public virtual ImportBrand ImportBrand { get; set; }
    }
}
