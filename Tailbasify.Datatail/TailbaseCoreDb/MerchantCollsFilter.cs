//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class MerchantCollsFilter
    {
        public int deptId { get; set; }
        public int Id_spec { get; set; }
        public string spec { get; set; }
        public string unit { get; set; }
        public int Id_langue { get; set; }
        public string List { get; set; }
        public int ordre { get; set; }
        public bool isFilter { get; set; }
    }
}
