//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class merchantUser
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public merchantUser()
        {
            this.ReviewsResponses = new HashSet<ReviewsRespons>();
            this.merchantUsersStores = new HashSet<merchantUsersStore>();
            this.WebsiteRequestFollowups = new HashSet<WebsiteRequestFollowup>();
        }
    
        public int ID { get; set; }
        public int merchant_ID { get; set; }
        public string f_name { get; set; }
        public string l_name { get; set; }
        public string email_user { get; set; }
        public string pwd_user { get; set; }
        public bool admin { get; set; }
        public string cats { get; set; }
        public bool featured { get; set; }
        public bool xt_warranty { get; set; }
        public bool active { get; set; }
        public bool catalogue { get; set; }
        public bool quoteToolSales { get; set; }
        public bool quoteToolAdmin { get; set; }
        public string merchantStoreIds { get; set; }
        public bool quoteToolSuperAdmin { get; set; }
        public bool OPRUser { get; set; }
        public Nullable<bool> seoTools { get; set; }
        public Nullable<bool> blogTools { get; set; }
    
        public virtual merchant merchant { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ReviewsRespons> ReviewsResponses { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<merchantUsersStore> merchantUsersStores { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WebsiteRequestFollowup> WebsiteRequestFollowups { get; set; }
    }
}
