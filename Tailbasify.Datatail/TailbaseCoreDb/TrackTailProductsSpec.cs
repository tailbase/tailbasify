//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class TrackTailProductsSpec
    {
        public int ID { get; set; }
        public int productID { get; set; }
        public string spec { get; set; }
        public string value { get; set; }
        public int Id_langue { get; set; }
        public int orderDisplay { get; set; }
    
        public virtual TrackTailProduct TrackTailProduct { get; set; }
    }
}
