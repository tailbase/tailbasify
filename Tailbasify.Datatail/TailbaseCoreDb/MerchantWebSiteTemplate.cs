//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class MerchantWebSiteTemplate
    {
        public int id_websitetemplate { get; set; }
        public int merchant_id { get; set; }
        public Nullable<int> sshowImageWidth { get; set; }
        public Nullable<int> sshowImageHeight { get; set; }
        public Nullable<int> HomeADQuantity { get; set; }
        public string HomeADDimensions { get; set; }
        public string CustomSSImageBank { get; set; }
    }
}
