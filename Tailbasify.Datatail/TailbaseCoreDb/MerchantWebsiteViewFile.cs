//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class MerchantWebsiteViewFile
    {
        public int ID { get; set; }
        public int MerchantID { get; set; }
        public string Directory { get; set; }
        public string FileName { get; set; }
        public int Size { get; set; }
        public System.DateTime FileDateLastModified { get; set; }
        public System.DateTime CreationDate { get; set; }
        public System.DateTime ModificationDate { get; set; }
    }
}
