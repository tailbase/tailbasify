//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class MerchantGiftCard
    {
        public int id { get; set; }
        public int merchant_id { get; set; }
        public string name_1 { get; set; }
        public string name_2 { get; set; }
        public decimal amount { get; set; }
    }
}
