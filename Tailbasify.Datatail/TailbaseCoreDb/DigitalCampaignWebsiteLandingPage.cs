//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class DigitalCampaignWebsiteLandingPage
    {
        public int DigitalWebsiteID { get; set; }
        public int PlaceholderID { get; set; }
        public Nullable<int> DigitalCampaignsLandingPagesID { get; set; }
    
        public virtual DigitalCampaignsLandingPages_rename_delete_20190109 DigitalCampaignsLandingPages_rename_delete_20190109 { get; set; }
        public virtual DigitalWebsite DigitalWebsite { get; set; }
    }
}
