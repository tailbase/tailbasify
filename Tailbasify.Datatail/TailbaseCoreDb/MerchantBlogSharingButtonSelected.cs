//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class MerchantBlogSharingButtonSelected
    {
        public int MerchantBlogID { get; set; }
        public int MerchantBlogSharingButtonID { get; set; }
        public string Location { get; set; }
    
        public virtual MerchantBlog MerchantBlog { get; set; }
        public virtual MerchantBlogSharingButtonType MerchantBlogSharingButtonType { get; set; }
    }
}
