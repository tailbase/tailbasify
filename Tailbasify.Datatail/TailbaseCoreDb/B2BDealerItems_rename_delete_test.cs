//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class B2BDealerItems_rename_delete_test
    {
        public string DealerID { get; set; }
        public int MerchantID { get; set; }
        public int PriceTypeID { get; set; }
        public int TailbaseID { get; set; }
        public System.DateTime CreationDate { get; set; }
    
        public virtual B2BDealers_rename_delete_test B2BDealers_rename_delete_test { get; set; }
    }
}
