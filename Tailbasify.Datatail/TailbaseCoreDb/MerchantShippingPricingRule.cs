//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class MerchantShippingPricingRule
    {
        public int merchantShippingID { get; set; }
        public Nullable<decimal> max { get; set; }
        public Nullable<decimal> min { get; set; }
        public Nullable<decimal> flat { get; set; }
        public Nullable<decimal> percentage { get; set; }
    
        public virtual MerchantShipping MerchantShipping { get; set; }
    }
}
