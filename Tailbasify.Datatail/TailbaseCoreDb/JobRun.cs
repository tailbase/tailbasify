//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class JobRun
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public JobRun()
        {
            this.JobRunReports = new HashSet<JobRunReport>();
        }
    
        public int Id { get; set; }
        public string JobDescription { get; set; }
        public string JobOutput { get; set; }
        public Nullable<bool> DryRun { get; set; }
        public System.DateTime Time { get; set; }
        public Nullable<int> state { get; set; }
        public Nullable<int> FeedId { get; set; }
        public Nullable<int> MerchantFeedId { get; set; }
        public string Report { get; set; }
        public string ReportSummary { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobRunReport> JobRunReports { get; set; }
    }
}
