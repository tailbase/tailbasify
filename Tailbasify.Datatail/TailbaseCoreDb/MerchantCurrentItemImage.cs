//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class MerchantCurrentItemImage
    {
        public int MerchantId { get; set; }
        public int ItemType { get; set; }
        public int ItemId { get; set; }
        public string FileName { get; set; }
        public Nullable<int> Size { get; set; }
        public Nullable<System.DateTime> FileDateLastModified { get; set; }
        public System.DateTime CreationDate { get; set; }
    }
}
