//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class Sections_Translations
    {
        public int Id { get; set; }
        public int Id_section { get; set; }
        public int Id_langue { get; set; }
        public string section { get; set; }
        public Nullable<bool> record_authority { get; set; }
    }
}
