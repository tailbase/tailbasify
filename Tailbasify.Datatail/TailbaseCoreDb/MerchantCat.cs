//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class MerchantCat
    {
        public int ID { get; set; }
        public int Merchant_ID { get; set; }
        public int catID { get; set; }
        public string Manufs { get; set; }
        public bool Datatail { get; set; }
        public string dept_code { get; set; }
        public string dept_1 { get; set; }
        public string dept_2 { get; set; }
        public Nullable<int> productId { get; set; }
        public Nullable<int> MerchantDepartmentID { get; set; }
        public System.DateTime CreationDate { get; set; }
        public System.DateTime ModifDate { get; set; }
        public bool dynamic_pricing { get; set; }
        public bool showIconInNavigation { get; set; }
        public bool showGallery { get; set; }
        public string tips { get; set; }
        public bool showTransitionPage { get; set; }
        public Nullable<byte> display_order { get; set; }
        public string slug { get; set; }
        public string category_1 { get; set; }
        public string category_2 { get; set; }
        public int SearchSuggestPriority { get; set; }
        public Nullable<int> groupCatID { get; set; }
    
        public virtual MerchantDepartment MerchantDepartment { get; set; }
        public virtual merchant merchant { get; set; }
    }
}
