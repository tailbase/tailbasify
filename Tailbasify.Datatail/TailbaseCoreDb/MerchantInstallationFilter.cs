//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class MerchantInstallationFilter
    {
        public int id { get; set; }
        public int MerchantInstallationId { get; set; }
        public Nullable<int> catId { get; set; }
        public Nullable<int> subCatId { get; set; }
        public Nullable<int> brandId { get; set; }
        public Nullable<int> collectionId { get; set; }
        public Nullable<int> productId { get; set; }
    }
}
