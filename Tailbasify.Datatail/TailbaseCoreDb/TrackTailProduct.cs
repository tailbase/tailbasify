//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class TrackTailProduct
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TrackTailProduct()
        {
            this.TrackTailProductsSpecs = new HashSet<TrackTailProductsSpec>();
        }
    
        public int ID { get; set; }
        public int clientID { get; set; }
        public string model { get; set; }
        public Nullable<int> brandID { get; set; }
        public string brand { get; set; }
        public Nullable<int> catID { get; set; }
        public string category_en { get; set; }
        public string category_fr { get; set; }
        public string subcategory_en { get; set; }
        public string subcategory_fr { get; set; }
        public decimal advPrice { get; set; }
        public Nullable<decimal> regPrice { get; set; }
        public Nullable<decimal> msrp { get; set; }
        public string web_address { get; set; }
        public System.DateTime dateRegistered { get; set; }
        public Nullable<decimal> fee { get; set; }
    
        public virtual Company Company { get; set; }
        public virtual TrackTailSolutions_Clients TrackTailSolutions_Clients { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TrackTailProductsSpec> TrackTailProductsSpecs { get; set; }
    }
}
