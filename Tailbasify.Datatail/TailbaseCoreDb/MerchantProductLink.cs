//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class MerchantProductLink
    {
        public int id { get; set; }
        public Nullable<int> merchant_id { get; set; }
        public string productid { get; set; }
        public string img_src { get; set; }
        public string img_alt { get; set; }
        public string link_url { get; set; }
        public string link_text { get; set; }
        public Nullable<System.DateTime> startDate { get; set; }
        public Nullable<System.DateTime> endDate { get; set; }
        public System.DateTime updated_at { get; set; }
    }
}
