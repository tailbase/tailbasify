//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class webTarget
    {
        public int ID { get; set; }
        public string target { get; set; }
        public string targetUrl { get; set; }
        public string allCatsUrl { get; set; }
        public Nullable<bool> allCatsInHomePage { get; set; }
        public string xPathForDepts { get; set; }
        public string xPathForCats { get; set; }
        public string deptsUrlExtractor { get; set; }
        public string catsUrlExtractor { get; set; }
        public string ajaxUrl { get; set; }
        public bool active { get; set; }
        public Nullable<int> id_cie { get; set; }
        public string dirPath { get; set; }
        public string moduleForHandlingQualifiers { get; set; }
        public Nullable<bool> isJSON { get; set; }
        public string xPathForIdProduct { get; set; }
        public string xPathForPrice { get; set; }
        public string xPathForModel { get; set; }
        public string xPathForSKU { get; set; }
        public string xPathForPriceQualifier { get; set; }
        public string xPathForCheckOutItems { get; set; }
        public string xPathForShoppingCartItems { get; set; }
        public string xPathForAltPageItems { get; set; }
        public string idProductExtractor { get; set; }
        public string priceExtractor { get; set; }
        public string modelExtractor { get; set; }
        public string skuExtractor { get; set; }
        public string checkOutTypeExtractor { get; set; }
        public string shoppingCartTypeExtractor { get; set; }
        public string altPageTypeExtractor { get; set; }
        public string checkOutUrl { get; set; }
        public string checkOutPriceExtractor { get; set; }
        public string checkOutJoinField { get; set; }
        public string checkOutJoinExtractor { get; set; }
        public string shoppingCartUrl { get; set; }
        public string shoppingCartPriceExtractor { get; set; }
        public string shoppingCartJoinField { get; set; }
        public string shoppingCartJoinExtractor { get; set; }
        public string keyPhrasesForPriceQualifier { get; set; }
        public string altPageUrl { get; set; }
        public string altPagePriceExtractor { get; set; }
        public string altPageJoinField { get; set; }
        public string altPageJoinExtractor { get; set; }
        public string allProdsParam { get; set; }
        public Nullable<int> maxProdsPerPage { get; set; }
        public string nPageParam { get; set; }
        public string requiredSufixForPageNav { get; set; }
        public Nullable<bool> modelInDetailPage { get; set; }
        public string detailPageUrl { get; set; }
        public string detailPageParamForIdProd { get; set; }
        public string uniqueModelPrefix { get; set; }
        public Nullable<bool> useRawCFHTTP { get; set; }
    
        public virtual Company Company { get; set; }
    }
}
