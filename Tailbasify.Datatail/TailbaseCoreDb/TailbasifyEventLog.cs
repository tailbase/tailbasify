//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class TailbasifyEventLog
    {
        public int ID { get; set; }
        public int MerchantId { get; set; }
        public string Module { get; set; }
        public string Category { get; set; }
        public string Text { get; set; }
        public Nullable<System.DateTime> Time { get; set; }
        public Nullable<int> Severity { get; set; }
        public string Location { get; set; }
        public string FunctionName { get; set; }
        public string RequestUrl { get; set; }
    
        public virtual merchant merchant { get; set; }
    }
}
