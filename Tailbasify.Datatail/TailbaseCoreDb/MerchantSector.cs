//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class MerchantSector
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MerchantSector()
        {
            this.MerchantDepartments = new HashSet<MerchantDepartment>();
        }
    
        public int ID { get; set; }
        public int Merchant_ID { get; set; }
        public string sector_1 { get; set; }
        public string sector_2 { get; set; }
        public Nullable<byte> homeAd { get; set; }
        public Nullable<byte> display_order { get; set; }
        public System.DateTime dateCreation { get; set; }
        public System.DateTime dateModification { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MerchantDepartment> MerchantDepartments { get; set; }
        public virtual merchant merchant { get; set; }
    }
}
