//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class Feeds1
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Feeds1()
        {
            this.Locations = new HashSet<Location>();
            this.MerchantFeeds1 = new HashSet<MerchantFeeds1>();
            this.PricingTiers = new HashSet<PricingTier>();
            this.ProductBasePrices = new HashSet<ProductBasePrice>();
            this.Medias = new HashSet<Media>();
        }
    
        public int Id { get; set; }
        public string Options { get; set; }
        public string Name { get; set; }
        public string ClassName { get; set; }
        public string UpdateFrequency { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Location> Locations { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MerchantFeeds1> MerchantFeeds1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PricingTier> PricingTiers { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductBasePrice> ProductBasePrices { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Media> Medias { get; set; }
    }
}
