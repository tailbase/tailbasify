//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class AccordDFinancingPlanType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AccordDFinancingPlanType()
        {
            this.MerchantAccordDFinancingPlans = new HashSet<MerchantAccordDFinancingPlan>();
        }
    
        public byte ID { get; set; }
        public string FinancingType { get; set; }
        public string Name_1 { get; set; }
        public string Name_2 { get; set; }
        public string MonthsKey { get; set; }
        public string MonthsLabel_1 { get; set; }
        public string MonthsLabel_2 { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MerchantAccordDFinancingPlan> MerchantAccordDFinancingPlans { get; set; }
    }
}
