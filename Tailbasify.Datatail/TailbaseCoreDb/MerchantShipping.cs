//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class MerchantShipping
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MerchantShipping()
        {
            this.MerchantShippingPrices = new HashSet<MerchantShippingPrice>();
            this.MerchantShippingPricesByQuantities = new HashSet<MerchantShippingPricesByQuantity>();
            this.MerchantShippingPricesByStoreLocRadios = new HashSet<MerchantShippingPricesByStoreLocRadio>();
            this.MerchantShippingPricesByStoreLocRings = new HashSet<MerchantShippingPricesByStoreLocRing>();
            this.MerchantShippingPricesByValueRanges = new HashSet<MerchantShippingPricesByValueRanx>();
        }
    
        public int id { get; set; }
        public int merchant_id { get; set; }
        public string name_1 { get; set; }
        public string name_2 { get; set; }
        public string type { get; set; }
        public string comment { get; set; }
        public Nullable<decimal> pickupPrice { get; set; }
        public Nullable<byte> delayMinimumDays { get; set; }
        public Nullable<byte> delayMaximumDays { get; set; }
        public string apiKey { get; set; }
        public Nullable<double> markupAmount { get; set; }
        public string markupType { get; set; }
        public Nullable<bool> pickupoldapp { get; set; }
        public Nullable<int> dropShippingProgramID { get; set; }
        public Nullable<bool> UnionStrategy { get; set; }
    
        public virtual MerchantShippingExcludeRule MerchantShippingExcludeRule { get; set; }
        public virtual MerchantShippingIncludeRule MerchantShippingIncludeRule { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MerchantShippingPrice> MerchantShippingPrices { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MerchantShippingPricesByQuantity> MerchantShippingPricesByQuantities { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MerchantShippingPricesByStoreLocRadio> MerchantShippingPricesByStoreLocRadios { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MerchantShippingPricesByStoreLocRing> MerchantShippingPricesByStoreLocRings { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MerchantShippingPricesByValueRanx> MerchantShippingPricesByValueRanges { get; set; }
        public virtual MerchantShippingPricingRule MerchantShippingPricingRule { get; set; }
    }
}
