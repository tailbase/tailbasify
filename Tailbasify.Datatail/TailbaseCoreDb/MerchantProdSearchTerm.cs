//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class MerchantProdSearchTerm
    {
        public int ProductId { get; set; }
        public int Merchant_Id { get; set; }
        public string SearchTerm_1 { get; set; }
        public string SearchTerm_2 { get; set; }
        public string TermType { get; set; }
        public int Counter { get; set; }
    
        public virtual merchant merchant { get; set; }
        public virtual Product Product { get; set; }
    }
}
