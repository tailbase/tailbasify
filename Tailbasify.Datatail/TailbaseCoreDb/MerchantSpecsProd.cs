//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class MerchantSpecsProd
    {
        public int ID { get; set; }
        public int specID { get; set; }
        public int productID { get; set; }
        public int cieId { get; set; }
        public int subCatID { get; set; }
        public string SpecValue_1 { get; set; }
        public string SpecValue_2 { get; set; }
        public int catID { get; set; }
        public int Merchant_ID { get; set; }
    }
}
