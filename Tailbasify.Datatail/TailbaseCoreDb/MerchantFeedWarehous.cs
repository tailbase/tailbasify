//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class MerchantFeedWarehous
    {
        public int id { get; set; }
        public int merchant_id { get; set; }
        public string brand { get; set; }
        public int productID { get; set; }
        public int warehouse1 { get; set; }
        public int warehouse2 { get; set; }
        public int warehouse3 { get; set; }
        public System.DateTime creationDate { get; set; }
        public System.DateTime modifDate { get; set; }
        public Nullable<decimal> InstantRebate1 { get; set; }
        public Nullable<System.DateTime> InstantRebate1End { get; set; }
        public Nullable<decimal> InstantRebate2 { get; set; }
        public string InstantRebate2Dates { get; set; }
        public Nullable<decimal> Cost { get; set; }
        public Nullable<int> POw1 { get; set; }
        public Nullable<int> POw2 { get; set; }
        public Nullable<int> POw3 { get; set; }
        public Nullable<int> POw4 { get; set; }
        public Nullable<bool> MultiPrice { get; set; }
        public Nullable<int> BB { get; set; }
        public string BBdates { get; set; }
        public Nullable<int> EOM1 { get; set; }
        public string EOM1dates { get; set; }
        public Nullable<int> EOM2 { get; set; }
        public string EOM2dates { get; set; }
        public Nullable<int> EOM3 { get; set; }
        public string EOM3dates { get; set; }
        public Nullable<int> MVP { get; set; }
        public string MVPdates { get; set; }
        public Nullable<bool> Active { get; set; }
    
        public virtual merchant merchant { get; set; }
        public virtual Product Product { get; set; }
    }
}
