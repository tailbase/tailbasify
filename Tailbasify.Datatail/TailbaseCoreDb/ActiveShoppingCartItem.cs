//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class ActiveShoppingCartItem
    {
        public int CartId { get; set; }
        public byte ItemType { get; set; }
        public int ItemId { get; set; }
        public int Qty { get; set; }
        public decimal ItemPrice { get; set; }
        public System.DateTime CreationDate { get; set; }
        public System.DateTime ModificationDate { get; set; }
    
        public virtual ActiveShoppingCart ActiveShoppingCart { get; set; }
    }
}
