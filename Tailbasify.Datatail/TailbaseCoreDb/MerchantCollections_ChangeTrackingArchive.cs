//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class MerchantCollections_ChangeTrackingArchive
    {
        public long Id { get; set; }
        public int MerchantId { get; set; }
        public Nullable<int> CollectionId { get; set; }
        public string ChangeType { get; set; }
        public System.DateTime ChangeTime { get; set; }
        public string ChangeByUser { get; set; }
        public string ChangeByApp { get; set; }
        public bool Lock { get; set; }
        public Nullable<bool> LockBefore { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> PriceBefore { get; set; }
        public Nullable<decimal> ReducedPrice { get; set; }
        public Nullable<decimal> ReducedPriceBefore { get; set; }
        public Nullable<decimal> Cost { get; set; }
        public Nullable<decimal> CostBefore { get; set; }
    }
}
