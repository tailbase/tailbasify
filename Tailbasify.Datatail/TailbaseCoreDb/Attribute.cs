//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class Attribute
    {
        public int Id_attribute { get; set; }
        public string AttributeConcat { get; set; }
        public string AttributeEn { get; set; }
        public string AttributeFr { get; set; }
        public Nullable<bool> record_authority { get; set; }
    }
}
