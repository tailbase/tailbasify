//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class CustomerAddress
    {
        public int id { get; set; }
        public int customerId { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string provinceCode { get; set; }
        public string postalCode { get; set; }
        public string countryCode { get; set; }
        public string telephone { get; set; }
        public string addressType { get; set; }
        public Nullable<int> merchantReplicationSourceId { get; set; }
        public Nullable<int> id_source { get; set; }
        public Nullable<int> customerId_source { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string companyName { get; set; }
    
        public virtual Customer Customer { get; set; }
    }
}
