//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class CollectionsSpec
    {
        public int ID { get; set; }
        public int Id_spec { get; set; }
        public int deptId { get; set; }
        public string sect { get; set; }
        public string spec { get; set; }
        public string format { get; set; }
        public string unit { get; set; }
        public string definition { get; set; }
        public int ordre { get; set; }
        public int Id_langue { get; set; }
        public string FormField { get; set; }
        public string List { get; set; }
        public bool filterField { get; set; }
        public bool isFilter { get; set; }
        public bool isCollectionFilter { get; set; }
        public string quickViewCats { get; set; }
        public bool width { get; set; }
        public bool height { get; set; }
        public bool depth { get; set; }
        public bool weight { get; set; }
        public System.DateTime DateCreation { get; set; }
        public System.DateTime DateModification { get; set; }
        public bool quickView { get; set; }
        public Nullable<bool> Visible { get; set; }
    }
}
