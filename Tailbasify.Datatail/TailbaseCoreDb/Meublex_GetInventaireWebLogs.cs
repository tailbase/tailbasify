//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tailbasify.Datatail.TailbaseCoreDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class Meublex_GetInventaireWebLogs
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string PTR { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> ReducedPrice { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<bool> Lock { get; set; }
        public int MerchantId { get; set; }
        public string Action { get; set; }
        public string Description { get; set; }
        public System.DateTime ImportRuntime { get; set; }
        public bool TestRun { get; set; }
    }
}
