﻿using ShopifySharp;
using ShopifySharp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tailbasify.Core
{
    public class Class1
    {

        public void Auth()
        {
            var service = new ProductService("https://sprint-15-test-api.myshopify.com/", "e3381fc8aaecd9f6c7e3c2b058cebaa7");


        }

        public Uri Authorize()
        {
            //This is the user's store URL.
            string usersMyShopifyUrl = "https://sprint-15-test-api.myshopify.com/";

            // A URL to redirect the user to after they've confirmed app installation.
            // This URL is required, and must be listed in your app's settings in your Shopify app dashboard.
            // It's case-sensitive too!
            string redirectUrl = "https://localhost:44353/";

            //An array of the Shopify access scopes your application needs to run.
            var scopes = new List<AuthorizationScope>()
            {
                AuthorizationScope.ReadCustomers,
                AuthorizationScope.WriteCustomers
            };


            //All AuthorizationService methods are static.
            return AuthorizationService.BuildAuthorizationUrl(scopes, usersMyShopifyUrl, "e3381fc8aaecd9f6c7e3c2b058cebaa7", redirectUrl);
        }


    }
}
