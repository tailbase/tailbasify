﻿using ShopifySharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tailbasify.Datatail.TailbaseCoreDb;

namespace Tailbasify.Core.Models
{
    public class TailbaseProduct
    {
        public int Id_product { get; set; }
        public string model { get; set; }
        public int manufID { get; set; }
        public bool ExtraModel { get; set; }
        public Nullable<int> ExtraModelID { get; set; }
        public Nullable<int> ProvID { get; set; }
        public bool photo { get; set; }
        public string description_1 { get; set; }
        public string description_2 { get; set; }
        public string descriptionUSA_1 { get; set; }
        public int catID { get; set; }
        public int subCatID { get; set; }
        public List<Category> category { get; set; }
        public List<Department> department { get; set; }
    }
}
