﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tailbasify.Core.Models
{
    public class Recurring
    {
        [JsonProperty("recurring_application_charge", NullValueHandling = NullValueHandling.Ignore)]
        public ChargeDetail Charge { get; set; }
    }

    public class ChargeDetail
    {
        public string name          {get; set;}
        public string price         {get; set;}
        public string return_url    {get; set;}
        public string trial_days    {get; set;}
        public string test { get; set; }
    }
}
