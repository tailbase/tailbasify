﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tailbasify.Core.Models.GraphQL
{
    public class GraphQLInputModel
    {
        public dynamic input { get; set; }
    }
}
