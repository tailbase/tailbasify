﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tailbasify.Core.Models.GraphQL
{
    public class PrivateMetafieldValueInput
    {
        public string value { get; set; }
        public string valueType { get; set; }
    }

    public enum PrivateMetafieldValueType
    { 
        INTEGER,
        JSON_STRING,
        STRING
    }
}
