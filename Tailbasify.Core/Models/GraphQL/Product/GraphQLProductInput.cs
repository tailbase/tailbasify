﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tailbasify.Core.Models.GraphQL.Product
{
    public class GraphQLProductInput
    {
        public string title { get; set; }     
        public string bodyHtml { get; set; }
        public string vendor { get; set; }     
        public string productType { get; set; }        
        //public string handle { get; set; }
        public bool published { get; set; }
        public string tags { get; set; }
        public List<GraphQLProductVariantInput> variants { get; set; }        
        public List<GraphQLProductImageInput> images { get; set; }
        public List<PrivateMetafieldInput> privateMetafields  { get; set; }
    }
}
