﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tailbasify.Core.Models.GraphQL.Product
{
    public class GraphQLProductVariantInput
    {
        public decimal? compareAtPrice { get; set; }
        public decimal? price { get; set; }
        //public List<MetafieldInput> metafields { get; set; }
        public bool taxable { get; set; }
    }
}
