﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tailbasify.Core.Models.GraphQL.Product
{
    public class GraphQLProductImageInput
    {
        //public string altText { get; set; }
        public string src { get; set; }
    }
}
