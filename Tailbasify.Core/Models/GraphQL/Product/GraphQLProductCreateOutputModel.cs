﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tailbasify.Core.Models.GraphQL.Product
{
    public class GraphQLProductCreateOutputModel
    {
        public string shopifyProductId { get; set; }

        public string tailbaseProductId { get; set; }
    }

}
