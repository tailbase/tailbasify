﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tailbasify.Core.Models.GraphQL
{
    public class PrivateMetafieldInput
    {
        public string key { get; set; }
        [JsonProperty(PropertyName = "namespace")]
        public string inputNamespace { get; set; }
        public PrivateMetafieldValueInput valueInput { get; set; }


    }
}
