﻿using GraphQL;
using GraphQL.Client.Abstractions;
using GraphQL.Client.Http;
using GraphQL.Client.Serializer.Newtonsoft;
using Newtonsoft.Json;
using ShopifySharp;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Tailbasify.Core.Models.GraphQL;
using Tailbasify.Core.Models.GraphQL.Product;

namespace Tailbasify.Core.Gateway
{
    public class ProductGateway
    {
        private string SHOPIFYSTOREURL;
        private string ACCESSTOKEN;
        Random random = new Random();
        //private ConcurrentBag<int> AVAILABLEPOINTS = new ConcurrentBag<int>(1000);
        

        public ProductGateway(string shopifyStoreUrl, string accessToken)
        {
            SHOPIFYSTOREURL = shopifyStoreUrl;
            ACCESSTOKEN = accessToken;
        }

        public List<GraphQLProductCreateOutputModel> PublishProducts(List<GraphQLProductInput> productsToPublish)
        {
            ConcurrentBag<GraphQLProductCreateOutputModel> publishedProducts = new ConcurrentBag<GraphQLProductCreateOutputModel>();

            /*
            Parallel.ForEach(productsToPublish, new ParallelOptions()
            {
                MaxDegreeOfParallelism = 10
            }, async (product) => {
                try
                {
                    Product publishedProduct = await PublishProduct(product);
                    await (async ()=> { 
                        publishedProducts.Add(publishedProduct);
                    })();
                }
                catch (Exception ex)
                {
                    
                }
            });
            */

            var performTask = new TransformBlock<GraphQLProductInput, GraphQLProductCreateOutputModel>(
            async (product) =>
            {
                GraphQLProductCreateOutputModel published = await PublishProduct(product);
                return published;
            }, new ExecutionDataflowBlockOptions
            {
                MaxDegreeOfParallelism = 13
                //DataflowBlockOptions.Unbounded
            });

            var addPublishedProducts = new ActionBlock<GraphQLProductCreateOutputModel>(p => publishedProducts.Add(p));
            performTask.LinkTo(
                addPublishedProducts, new DataflowLinkOptions
                {
                    PropagateCompletion = true
                });

            foreach (GraphQLProductInput p in productsToPublish)
                performTask.Post(p);

            performTask.Complete();
            addPublishedProducts.Completion.Wait();

            return publishedProducts.ToList();
        }

        private async Task<GraphQLProductCreateOutputModel> PublishProduct(GraphQLProductInput product)
        {
            ServicePointManager.ServerCertificateValidationCallback +=
                (sender, cert, chain, sslPolicyErrors) => true;

            var client = new GraphQLHttpClient($"https://{SHOPIFYSTOREURL}/admin/api/2020-07/graphql.json",
                new NewtonsoftJsonSerializer());

            client.HttpClient.DefaultRequestHeaders.Add("X-Shopify-Access-Token", ACCESSTOKEN);

            GraphQLInputModel inputModel = new GraphQLInputModel();
            inputModel.input = product;

            var request = new GraphQLRequest()
            {
                Query = @"
                        mutation productCreate($input: ProductInput!) {
                          productCreate(input: $input) {
                            product {
                              id
                              privateMetafields(first:1) {
                                edges {
                                    node {
                                        value
                                    }
                                }
                              }   
                            }
                            shop {
                              id
                            }
                            userErrors {
                              field
                              message
                            }
                          }
                        }",
                OperationName = "productCreate",
                Variables = inputModel
            };

            var response = await client.SendMutationAsync<object>(request);
            dynamic data = response.Data;

            GraphQLProductCreateOutputModel productCreateOutput = new GraphQLProductCreateOutputModel();
            productCreateOutput = GetGraphQLProductCreateOutputModel(data);
            string responseJson = JsonConvert.SerializeObject(response);

            Debug.WriteLine(responseJson);
            //int randomNumber = random.Next(1000, 1050);
            //Thread.Sleep(randomNumber);
            return productCreateOutput;


            /*
            var service = new ProductService(SHOPIFYSTOREURL, ACCESSTOKEN);
            return await service.CreateAsync(product);
            */
        }

        private GraphQLProductCreateOutputModel GetGraphQLProductCreateOutputModel(dynamic data)
        {
            GraphQLProductCreateOutputModel output = new GraphQLProductCreateOutputModel();

            if(data.productCreate != null && data.productCreate.product != null)
            {
                var product = data.productCreate.product;
                output.shopifyProductId = product.id;
                output.tailbaseProductId = product.privateMetafields.edges[0].node.value;
            }

            return output;
        }
    }
}
