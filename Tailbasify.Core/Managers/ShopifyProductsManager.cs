﻿using ShopifySharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Tailbasify.Core.Gateway;
using Tailbasify.Core.Models;
using Tailbasify.Core.Models.GraphQL;
using Tailbasify.Core.Models.GraphQL.Product;
using Tailbasify.Datatail.TailbaseCoreDb;
using Product = ShopifySharp.Product;

namespace Tailbasify.Core.Managers
{
    public class ShopifyProductsManager : BaseManager
    {
        ShopifyMerchantManager shopifyMerchantManager = new ShopifyMerchantManager();
        MerchantProdsManager merchantProdsManager = new MerchantProdsManager();
        ImagesManager imagesManager = new ImagesManager();

        public async Task SyncProductsAsync(int? merchantId)
        {
            merchantId = 534;

            // Get merchant prods 
            List<ShopifyMerchantProdsExtended> merchantProds = merchantProdsManager.GetProducts(merchantId);
            //Get Product Specs

            // Get product images
            List<ItemImagesHD> productImages = imagesManager.GetImages(merchantProds.Select(mp => mp.ProductID).ToList());

            // Get merchant prods to add list
            List<ShopifyMerchantProdsExtended> toAdd = merchantProds.Where(mp => mp.ShopifyProductId == null).ToList();

            List<ShopifyMerchantProdsExtended> toUpdate = merchantProds.Where(mp => mp.ShopifyProductId != null).ToList();

            //List<Product> shopifyProductsToAdd = MapMerchantProdsToShopify(toAdd, productImages);
            //List<Product> shopifyProductsToUpdate = MapMerchantProdsToShopify(toUpdate, productImages);

            List<GraphQLProductInput> productsToAdd = MapMerchantProdsToShopifyGraphQL(toAdd, productImages);

            string shopifyStoreUrl = shopifyMerchantManager.GetShopifyStoreURL(merchantId.Value);
            string accessToken = shopifyMerchantManager.GetToken(merchantId.Value);

            ProductGateway productGateway = new ProductGateway(shopifyStoreUrl, accessToken);
            List<GraphQLProductCreateOutputModel> publishedProducts = productGateway.PublishProducts(productsToAdd);

            merchantProdsManager.UpdateShopifyProductId(merchantId.Value, publishedProducts);
        }

        #region GraghQL Mapping
        private List<GraphQLProductInput> MapMerchantProdsToShopifyGraphQL(List<ShopifyMerchantProdsExtended> merchantProds, List<ItemImagesHD> images)
        {
            List<GraphQLProductInput> graphQLProducts = new List<GraphQLProductInput>();

            foreach (ShopifyMerchantProdsExtended merchantProd in merchantProds)
            {
                List<GraphQLProductImageInput> productImages = MapMerchantProdImagesToShopifyGraphQL(merchantProd, images.Where(i => i.ItemID == merchantProd.ProductID).ToList());
                GraphQLProductInput shopifyProduct = new GraphQLProductInput()
                {
                    title = $"{merchantProd.cie} {merchantProd.model} ({merchantProd.category} - {merchantProd.SubCategory})",
                    bodyHtml = merchantProd.description_1,
                    vendor = merchantProd.cie,
                    published = true,
                    tags = String.Join(",", new string[] { merchantProd.category, merchantProd.SubCategory + " " + merchantProd.category, merchantProd.model, merchantProd.cie, merchantProd.dept }),
                    variants = new List<GraphQLProductVariantInput>()
                    {
                        new GraphQLProductVariantInput()
                        {
                            compareAtPrice = GetPrice(merchantProd, "compareAtPrice"),
                            price = GetPrice(merchantProd, "price"),
                            taxable = true
                        }
                    },
                    images = productImages,
                    privateMetafields = new List<PrivateMetafieldInput>()
                    {
                        new PrivateMetafieldInput()
                        {
                            key = "TailbaseId",
                            inputNamespace = "Product",
                            valueInput = new PrivateMetafieldValueInput()
                            {
                                value = merchantProd.ProductID.ToString(),
                                valueType = PrivateMetafieldValueType.STRING.ToString()
                            }
                        }
                    }
                };
                graphQLProducts.Add(shopifyProduct);
            }
            return graphQLProducts;
        }

        private List<GraphQLProductImageInput> MapMerchantProdImagesToShopifyGraphQL(ShopifyMerchantProdsExtended merchantProd, List<ItemImagesHD> itemImages)
        {
            List<GraphQLProductImageInput> shopifyProductImages = new List<GraphQLProductImageInput>();

            foreach (ItemImagesHD itemImage in itemImages)
            {
                string imagePosition = Regex.Matches(itemImage.FileName, "_(.*).jpg")[0].Groups[1].Value;
                GraphQLProductImageInput shopifyProductImage = new GraphQLProductImageInput()
                {
                    src = $"https://imgres.tailbase.com/originalimg/prods/{itemImage.FileName}"
                    //TODO 
                    //Check height and width
                };

                shopifyProductImages.Add(shopifyProductImage);
            }

            return shopifyProductImages;
        }
        #endregion

        private decimal? GetPrice(ShopifyMerchantProdsExtended merchantProd, string priceType)
        {
            if(priceType.Equals("compareAtPrice"))
            {
                if (merchantProd.price > merchantProd.reducedPrice)
                    return merchantProd.price;
                else
                    return merchantProd.reducedPrice;
            }
            else
            {
                if (merchantProd.price < merchantProd.reducedPrice)
                    return merchantProd.price;
                else
                    return merchantProd.reducedPrice;
            }
        }
        
        #region REST API Mapping
        private List<Product> MapMerchantProdsToShopify(List<MerchantProdsExtended> merchantProds, List<ItemImagesHD> images)
        {
            List<Product> shopifyProducts = new List<Product>();
            
            foreach(MerchantProdsExtended merchantProd in merchantProds)
            {
                List<ProductImage> productImages = MapMerchantProdImagesToShopify(merchantProd, images.Where(i => i.ItemID == merchantProd.ProductID).ToList());
                Product shopifyProduct = new Product()
                {                   
                    Title = $"{merchantProd.cie} {merchantProd.model} ({merchantProd.category} - {merchantProd.SubCategory})",
                    BodyHtml = merchantProd.description_1,
                    Vendor = merchantProd.cie,
                    CreatedAt = DateTime.Now,
                    PublishedScope = "web",
                    Tags = String.Join(",", new string[] { merchantProd.category, merchantProd.SubCategory + " " + merchantProd.category, merchantProd.model, merchantProd.cie }),
                    Variants = new List<ProductVariant>()
                    {
                        new ProductVariant()
                        {
                            CompareAtPrice = merchantProd.price,
                            Price = merchantProd.reducedPrice == null? merchantProd.price : merchantProd.reducedPrice,
                            ProductId = merchantProd.ID,
                            Taxable = true,
                            Id = merchantProd.ID
                        }
                    },
                    Images = productImages,

                };
                shopifyProducts.Add(shopifyProduct);
            }

            return shopifyProducts;

        }

        private List<ProductImage> MapMerchantProdImagesToShopify(MerchantProdsExtended merchantProd, List<ItemImagesHD> itemImages)
        {
            List<ProductImage> shopifyProductImages = new List<ProductImage>(); 

            foreach(ItemImagesHD itemImage in itemImages)
            {
                string imagePosition = Regex.Matches(itemImage.FileName, "_(.*).jpg")[0].Groups[1].Value;
                ProductImage shopifyProductImage = new ProductImage()
                {
                    Position = Int32.Parse(imagePosition),
                    Src = $"https://imgres.tailbase.com/originalimg/prods/{itemImage.FileName}"
                    //TODO 
                    //Check height and width
                };

                shopifyProductImages.Add(shopifyProductImage);
            }

            return shopifyProductImages;
        }
        #endregion 
    }
}
