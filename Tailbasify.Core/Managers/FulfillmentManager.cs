﻿using ShopifySharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tailbasify.Datatail.TailbaseCoreDb;

namespace Tailbasify.Core.Managers
{
    public class FulfillmentManager : BaseManager
    {
        
        MerchantManager merchantManager = new MerchantManager();
        ShopifyMerchantManager shopifyMerchantManager = new ShopifyMerchantManager();
        
        public void OffboardMerchant(string jsonData)
        {
            //TODO
        }

        public void OnboardMerchant(string shop, string accessToken)
        {
            //merchantManager.UpdateStoreProvider(shop, "Shopify");
            shopifyMerchantManager.SaveToken(shop, accessToken);
        }


        #region process charge
        /*
        public async Task<string> AskCustomerCharge(string shop, string accessToken)
        {
            var service = new RecurringChargeService(shop, accessToken);
            var charge = new RecurringCharge()
            {
                Name = "Application Fee",
                Price = 0,
                Test = true, //Marks this charge as a test, meaning it won't charge the shop owner.
                TrialDays = 21,
                ReturnUrl = $"https://{appUrl}/fulfillment/ChargeResult?shop={shop}"
            };

            charge = await service.CreateAsync(charge);

            return charge.ConfirmationUrl;
        }
        */
        #endregion

    }
}
