﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tailbasify.Datatail.Repositories;
using Tailbasify.Datatail.TailbaseCoreDb;

namespace Tailbasify.Core.Managers
{
    public class ShopifyMerchantManager : BaseManager
    {
        ShopifyMerchantRepository repository = new ShopifyMerchantRepository();


        /// <summary>
        /// Returns true if already onboarded or no merchant id
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public bool ValidateShop(string shop)
        {
            if (shop == null)
                return true;

            ShopifyMerchant merchant = repository.GetMerchant(shop);
            if (merchant == null)
                return false;
            else
                return true;
        }

        internal string GetToken(int merchantId)
        {
            ShopifyMerchant merchant = repository.GetMerchant(merchantId);
            if (merchant != null)
                return merchant.Token;
            else
                return null;
        }

        internal void SaveToken(string shop, string accessToken)
        {
            repository.SaveToken(shop, accessToken);
        }

        internal string GetShopifyStoreURL(int merchantId)
        {
            return repository.GetShopifyStoreURL(merchantId);
        }
    }
}
