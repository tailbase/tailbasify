﻿using Tailbasify.Logger;

namespace Tailbasify.Core.Managers
{
    public class BaseManager
    {
        public string apiKey = AppSettings.ShopifyApiKey;
        public string secretKey = AppSettings.ShopifySecretKey;
        public string appUrl = AppSettings.AppUrl;

        public EventLogger eventLogger = new EventLogger();
    }
}
