﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tailbasify.Datatail.Repositories;
using Tailbasify.Datatail.TailbaseCoreDb;

namespace Tailbasify.Core.Managers
{
    public class ImagesManager : BaseManager
    {
        ImagesRepository repository = new ImagesRepository();
        internal List<ItemImagesHD> GetImages(List<int> productIDs)
        {
            return repository.GetImages(productIDs);
        }
    }
}
