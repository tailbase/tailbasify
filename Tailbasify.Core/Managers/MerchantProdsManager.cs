﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tailbasify.Core.Models.GraphQL.Product;
using Tailbasify.Datatail.Repositories;
using Tailbasify.Datatail.TailbaseCoreDb;

namespace Tailbasify.Core.Managers
{
    public class MerchantProdsManager : BaseManager
    {
        MerchantProdsRepository repository = new MerchantProdsRepository();
        internal List<ShopifyMerchantProdsExtended> GetProducts(int? merchantId)
        {
            return repository.GetProducts(merchantId);
        }

        internal void UpdateShopifyProductId(int merchantId, List<GraphQLProductCreateOutputModel> publishedProducts)
        {
            Dictionary<int, long> productIdsMap = new Dictionary<int, long>();
            productIdsMap = publishedProducts
                .Where(p => p.tailbaseProductId != null)
                .ToDictionary(
                p => int.Parse(p.tailbaseProductId),
                p => long.Parse(p.shopifyProductId.Split('/').LastOrDefault())
            );

            repository.UpdateShopifyProductId(merchantId, productIdsMap);
        }
    }
}
