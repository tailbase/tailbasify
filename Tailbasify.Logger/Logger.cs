﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Tailbasify.Datatail.Repositories;
using Tailbasify.Datatail.TailbaseCoreDb;

namespace Tailbasify.Logger
{
    public enum LoggerLevel
    {
        Trace = 0,
        Debug = 1,
        Info = 2,
        Warn = 3,
        Error = 4,
        Fatal = 5
    }
    public class EventLogger
    {
        TailbasifyEventLogsRepository repository = new TailbasifyEventLogsRepository();
        public void Log(int MerchantId, string Module, string Category,  LoggerLevel level, Exception ex,
                [System.Runtime.CompilerServices.CallerMemberName] string FunctionName = "",
                [System.Runtime.CompilerServices.CallerFilePath] string Location = "",
                [System.Runtime.CompilerServices.CallerLineNumber] int LineNumber = 0,
                string RequestUrl = ""
            )
        {
            if (string.IsNullOrEmpty(Module))
            {
                Module = Assembly.GetCallingAssembly().GetName().Name;
            }
            Task t = new Task(() =>
            {
                LogInternal(MerchantId, Module, Category, GetInnerException(ex), level, BuildLocation(Location, LineNumber), FunctionName, RequestUrl);
            });
            t.Start();
            t.Wait();
        }

        public void Log(int MerchantId, string Module, string Category, LoggerLevel level, string message,
                [System.Runtime.CompilerServices.CallerMemberName] string FunctionName = "",
                [System.Runtime.CompilerServices.CallerFilePath] string Location = "",
                [System.Runtime.CompilerServices.CallerLineNumber] int LineNumber = 0,
                string RequestUrl = "")
        {
            // Async logging
            Task t = new Task(() =>
            {
                LogInternal(MerchantId, Module, Category, message, level, BuildLocation(Location, LineNumber), FunctionName, RequestUrl);
            });
            t.Start();
            t.Wait();
        }

        private void LogInternal(int MerchantId, string Module, string Category, string Message, LoggerLevel Level,  string Location, string FunctionName, string RequestUrl)
        {
            //Prepare log
            TailbasifyEventLog log = new TailbasifyEventLog()
            {
                MerchantId = MerchantId,
                Category = Category,                
                Text = Message,
                Time = DateTime.Now,
                Severity = (int)Level,
                Module = Module,
                Location = Location,
                FunctionName = FunctionName,
                RequestUrl = RequestUrl
            };

            repository.AddLog(log);

            /*
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://eventreactor.tailbase.com/");
                var response = client.PostAsJsonAsync("api/Logs", log).Result;
            }
            */
        }

        private static string BuildLocation(string SourceFilePath, int LineNumber)
        {
            var directoryInfo = new DirectoryInfo(SourceFilePath);
            string location = "";
            if (directoryInfo != null && directoryInfo.Parent != null)
            {
                if (directoryInfo.Parent.Parent != null)
                {
                    location += directoryInfo.Parent.Parent.Name + "/";
                }
                location += directoryInfo.Parent.Name + "/";
            }
            location = $"{location}{directoryInfo.Name}, Line {LineNumber}";
            return location;
        }

        private static string GetInnerException(Exception Ex, string StackTrace = "")
        {
            if (Ex.InnerException != null)
            {
                if (!string.IsNullOrEmpty(Ex.StackTrace))
                {
                    return string.Format("{0} {1}Inner Exception : {2}", Ex.Message, Environment.NewLine, GetInnerException(Ex.InnerException, Ex.StackTrace));
                }
                else
                {
                    return string.Format("{0} {1}Inner Exception : {2}", Ex.Message, Environment.NewLine, GetInnerException(Ex.InnerException, StackTrace));
                }
            }
            if (!string.IsNullOrEmpty(StackTrace))
                return string.Format("{0}{1}Stack trace : {2}", Ex.Message, Environment.NewLine, StackTrace);
            else
                return Ex.Message;
        }
    }
    public static class HttpClientExtensions
    {
        public static Task<HttpResponseMessage> PostAsJsonAsync<T>(
            this HttpClient httpClient, string url, T data)
        {
            var dataAsString = JsonConvert.SerializeObject(data);
            var content = new StringContent(dataAsString);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return httpClient.PostAsync(url, content);
        }

        public static async Task<T> ReadAsJsonAsync<T>(this HttpContent content)
        {
            var dataAsString = await content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(dataAsString);
        }
    }
    
}
