﻿using Newtonsoft.Json;
using RestSharp;
using ShopifySharp;
using ShopifySharp.Enums;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Tailbasify.Core;
using Tailbasify.Core.Managers;
using Tailbasify.Core.Models;
using Tailbasify.Datatail.TailbaseCoreDb;

namespace Tailbasify.Controllers
{
    public class FulfillmentController : Controller
    {
        private string apiKey = AppSettings.ShopifyApiKey;
        private string secretKey = AppSettings.ShopifySecretKey;
        private string appUrl = AppSettings.AppUrl;

        FulfillmentManager manager = new FulfillmentManager();
        ShopifyMerchantManager shopifyMerchantManager = new ShopifyMerchantManager();

        private static string REAPPLYKEY = "b4fb859b-974a-4ad6-ac39-d4f8013a937e";

        // GET: Fulfillment
        public ActionResult Install(string shop, string signature, string timestamp)
        {
            //check if shop is valid to install
            if(!shopifyMerchantManager.ValidateShop(shop))
                return RedirectToAction("About", "Home"); // TODO - display message to validate the shop url

            // A URL to redirect the user to after they've confirmed app installation.
            // This URL is required, and must be listed in your app's settings in your Shopify app dashboard.
            // It's case-sensitive too!
            string redirectUrl = $"https://{appUrl}/Fulfillment/Auth";

            //An array of the Shopify access scopes your application needs to run.
            // TODO - review access scopes
            var scopes = new List<AuthorizationScope>()
            {
                AuthorizationScope.ReadContent,
                AuthorizationScope.WriteContent,
                AuthorizationScope.ReadThemes,
                AuthorizationScope.WriteThemes,
                AuthorizationScope.ReadProducts,
                AuthorizationScope.WriteProducts,
                AuthorizationScope.ReadCustomers,
                AuthorizationScope.WriteCustomers,
                AuthorizationScope.WriteOrders,
                AuthorizationScope.ReadScriptTags,
                AuthorizationScope.WriteScriptTags,
                AuthorizationScope.ReadFulfillments,
                AuthorizationScope.WriteFulfillments,
                AuthorizationScope.ReadShipping,
                AuthorizationScope.WriteShipping,
                AuthorizationScope.ReadAnalytics,
                AuthorizationScope.ReadCheckouts,
                AuthorizationScope.WriteCheckouts,
                AuthorizationScope.ReadReports,
                AuthorizationScope.WriteReports,
                AuthorizationScope.ReadPriceRules,
                AuthorizationScope.WritePriceRules,
                AuthorizationScope.ReadInventory,
                AuthorizationScope.WriteInventory,
                AuthorizationScope.ReadProductListings,
                AuthorizationScope.ReadCollectionListings,
                AuthorizationScope.ReadDraftOrders,
                AuthorizationScope.WriteDraftOrders,
                AuthorizationScope.ReadMarketingEvents,
                AuthorizationScope.WriteMarketingEvents,
                AuthorizationScope.ReadResourceFeedbacks,
                AuthorizationScope.WriteResourceFeedbacks,
                AuthorizationScope.UnauthenticatedReadCollectionListings,
                AuthorizationScope.UnauthenticatedReadProductListings,
                AuthorizationScope.UnauthenticatedWriteCheckouts,
                AuthorizationScope.UnauthenticatedWriteCustomers,
                AuthorizationScope.UnauthenticatedReadContent,
                AuthorizationScope.ReadLocations
            };

            //All AuthorizationService methods are static.
            Uri authUrl = AuthorizationService.BuildAuthorizationUrl(scopes, shop, apiKey, redirectUrl);

            return Redirect(authUrl.ToString());
        }

        public async Task<ActionResult> Auth(string shop, string code, string reApplyKey)
        {
            var queryString = Request.QueryString.AllKeys
                .ToDictionary(k => k, k => Request.QueryString[k]);

            if (AuthorizationService.IsAuthenticRequest(queryString, secretKey) || reApplyKey.Equals(REAPPLYKEY))
            {
                //Request is authentic.                
                string accessToken = await AuthorizationService.Authorize(code, shop, apiKey, secretKey);

                manager.OnboardMerchant(shop, accessToken);
                
                var service = new WebhookService(shop, accessToken);
                Webhook hook = new Webhook()
                {
                    Address = $"https://{appUrl}/Fulfillment/Uninstall",
                    CreatedAt = DateTime.Now,
                    Format = "json",
                    Topic = "app/uninstalled",
                };

                //TODO - create webhook for uninstalling the app
                hook = await service.CreateAsync(hook);

                /*
                // We will not be charging the merchant yet.
                var customerChargeConfirmationUrl = await manager.AskCustomerCharge(shop, (string)accessToken);
                return Redirect(customerChargeConfirmationUrl);
                */

                return RedirectToAction("About", "Home");
            }
            else
            {
                //Request is not authentic and should not be acted on.
            }
            return View();
        }

        [HttpPost]
        public ActionResult Uninstall()
        {
            var req = Request.InputStream;
            var json = new StreamReader(req).ReadToEnd();

            //TODO - make this run on a thread
            manager.OffboardMerchant(json);

            return new HttpStatusCodeResult(200);
        }

        #region process charge
        /*
        public async Task<ActionResult> ChargeResult(string shop, long charge_id)
        {
            string accessToken = manager.GetToken();
            var service = new RecurringChargeService(shop, accessToken);

            var charge = await service.GetAsync(charge_id);

            if (charge.Status.Equals("declined"))
            {
                //send customer somewere
                return RedirectToAction("Contact", "Home");
            }
            else if (charge.Status.Equals("accepted"))
            {
                //save shop url
                manager.SaveShopUrl(shop);

                //save charge id
                //TODO

                //activate charge
                var activatedCharge = await service.ActivateAsync(charge_id);

                if (activatedCharge.Status.Equals("active"))
                {
                    manager.OnboardMerchant(shop);
                    //CUSTOMER ACCEPTED CHARGE GO TO APP
                    return RedirectToAction("About", "Home");
                }
                else
                {

                    //fail to actived

                    //send customer to error page
                    return RedirectToAction("Contact", "Home");
                }

            }
            else
            {
                return RedirectToAction("Contact", "Home");
            }
        }
        */
        #endregion
    }
}