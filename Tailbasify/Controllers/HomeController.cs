﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Tailbasify.Core;
using Tailbasify.Core.Managers;
using Tailbasify.Datatail.TailbaseCoreDb;

namespace Tailbasify.Controllers
{
    public class HomeController : Controller
    {
        CategoryManager categoryManager = new CategoryManager();
        ShopifyProductsManager storeProductsManager = new ShopifyProductsManager();
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        [Authorize]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [Authorize]
        public ActionResult Products(int? categoryId)
        {
            if(!categoryId.HasValue)
            {
                ViewBag.Categories = categoryManager.GetCategories(1);
                ViewBag.Products = new List<Product>();
            }
            else
            {
                ViewBag.Categories = new List<Category>();
                ViewBag.Products = categoryManager.GetProductsForCategory(categoryId.Value);
            }
            return View();
        }

        [Authorize]
        public async Task<ActionResult> SyncProducts(int? merchantId)
        {
            await storeProductsManager.SyncProductsAsync(merchantId);
            
            return RedirectToAction("About", "Home");
        }
    }
}