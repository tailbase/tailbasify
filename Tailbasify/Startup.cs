﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Tailbasify.Startup))]
namespace Tailbasify
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
