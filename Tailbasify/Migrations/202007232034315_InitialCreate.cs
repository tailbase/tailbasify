﻿namespace Tailbasify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Tailbasify.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "Tailbasify.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("Tailbasify.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("Tailbasify.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "Tailbasify.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "Tailbasify.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Tailbasify.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "Tailbasify.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("Tailbasify.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Tailbasify.AspNetUserRoles", "UserId", "Tailbasify.AspNetUsers");
            DropForeignKey("Tailbasify.AspNetUserLogins", "UserId", "Tailbasify.AspNetUsers");
            DropForeignKey("Tailbasify.AspNetUserClaims", "UserId", "Tailbasify.AspNetUsers");
            DropForeignKey("Tailbasify.AspNetUserRoles", "RoleId", "Tailbasify.AspNetRoles");
            DropIndex("Tailbasify.AspNetUserLogins", new[] { "UserId" });
            DropIndex("Tailbasify.AspNetUserClaims", new[] { "UserId" });
            DropIndex("Tailbasify.AspNetUsers", "UserNameIndex");
            DropIndex("Tailbasify.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("Tailbasify.AspNetUserRoles", new[] { "UserId" });
            DropIndex("Tailbasify.AspNetRoles", "RoleNameIndex");
            DropTable("Tailbasify.AspNetUserLogins");
            DropTable("Tailbasify.AspNetUserClaims");
            DropTable("Tailbasify.AspNetUsers");
            DropTable("Tailbasify.AspNetUserRoles");
            DropTable("Tailbasify.AspNetRoles");
        }
    }
}
